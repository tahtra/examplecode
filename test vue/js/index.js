// Эмулятор метода возращающего json
const endPoint = (count = 0) => {
	const itemCount = 9; // количество возращаемых элементов
	const jsonString = {
		"users": [
			{
				"id":1,
				"name":"Денис",
				"secondName":"Топорков",
				"age":30,
				"rating":115
			},
			{
				"id":2,
				"name":"Лукьян",
				"secondName":"Рагозин",
				"age":22,
				"rating":22
			},
			{
				"id":3,
				"name":"Ярослав",
				"secondName":"Рыжов",
				"age":45,
				"rating":10
			},
			{
				"id":4,
				"name":"Денис",
				"secondName":"Топорков",
				"age":55,
				"rating":102
			},
			{
				"id":5,
				"name":"Артем",
				"secondName":"Кашников",
				"age":22,
				"rating":97
			},
			{
				"id":6,
				"name":"Лада",
				"secondName":"Боярская",
				"age":25,
				"rating":15
			},
			{
				"id":7,
				"name":"Иосиф",
				"secondName":"Южанин",
				"age":41,
				"rating":0
			},
			{
				"id":8,
				"name":"Станислав",
				"secondName":"Качаев",
				"age":34,
				"rating":0
			},
			{
				"id":9,
				"name":"Эдуард",
				"secondName":"Бузинский",
				"age":32,
				"rating":200
			}
		]
	};
	let response = [];
	for (let i = count; i < count + itemCount; i++) {
		response.push(jsonString.users[i]);
	}
	return response;
};

// Поиск
Vue.component('search-input', {
	props: ['show', 'searchString'],
	template: `
		<div class="search">
			<transition name="slide-fade">
				<input
					class="search__input"
					v-show="show"
					v-bind:value="searchString"
					v-on:input="$emit('input', $event.target.value)"
				>
			</transition>
			<button class="search__icon" v-on:click="toggleSearch()"></button>
		</div>
	`,
	methods: {
		toggleSearch() {
			example.showSearch = !example.showSearch;
		}
	}
})

// Сортировка
Vue.component('sortable', {
	props: ['checkedFilter'],
	template: `
		<div class="sortable"
			v-on:click.prevent
			v-bind:data-checked="checkedFilter"
		>
			<a class="age" v-on:click="makeActive('age')">По возрасту</a> | <a class="rating" v-on:click="makeActive('rating')">По рейтингу</a>
		</div>
	`,
	methods: {
		makeActive(item) {
			example.checkedFilter = item
			example.sortParam = item
		}
	}
})

Vue.component('app-content', {
	props: ['todo', 'index'],
	template: `
		<article class="item">
			<div class="item__number">{{ index + 1 }}</div>
			<div class="item__image">{{ todo.name[0] }}{{ todo.secondName[0] }}</div>
			<div class="item__description">
				<div class="item__title">{{ todo.name }} {{ todo.secondName }}</div>
				<div class="item__age">Возраст: {{ todo.age }}</div>
			</div>
			<div class="item__rating">{{ todo.rating }}</div>
		</article>
	`
})

const example = new Vue({
	el: '#example',
	data: {
		sortParam: '',
		usersList: endPoint(0),
		checkedFilter: '',
		showSearch: false,
		searchString: '',
	},
	computed: {
		filteredList() {
			var comp = this.searchString;
			return this.usersList.filter(function(elem) {
				if (comp === '') return true;
				else return elem.secondName.indexOf(comp) > -1 || elem.name.indexOf(comp) > -1;
			})
		},
		sortedList() {
			switch(this.sortParam) {
				case 'age': return this.filteredList.sort(sortByAge);
				case 'rating': return this.filteredList.sort(sortByRating);
				default: return this.filteredList;
			}
		},
	}
})

const sortByAge = function (d1, d2) {
	return (Number(d1.age) > Number(d2.age)) ? 1 : -1;
};

const sortByRating = function (d1, d2) {
	return (Number(d1.rating) > Number(d2.rating)) ? 1 : -1;
};
