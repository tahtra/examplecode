<?php

require_once 'inc/files/Files.php';

/**
 * Контроллер для конструктора лендингов"
 *
 * @author Viktor Vorobyev, Dmitry Evtushenko
 * @version 1.0, 27.02.2018
 * @package livemaster
 */
class LandingController extends CommonController {

	/** Модель конструктора лендингов */
	private $_LandingManager;

	/** @var array Таргет-группа для прямого доступа к конструктору */
	private $_targetGroup = [
		4883531, //Николай Зайцев
		3060787  //Валентина Торяник
	];
	
	/**
	 * Конструктор контроллера
	 * 
	 * @author Viktor Vorobyev
	 * @version 1.0, 27.02.2018
	 * 
	 * @param string $action Метод контроллера
	 * @param array $params Данные, передаваемые в метод
	 */
	public function __construct($action, $params) {
		parent::__construct($action, $params);

		//Закрываем доступ ко всем методам контроллера, кроме метода index()
		if ($action != 'index') {
			$this->locationRedirect('/', !($this->User && ($this->User->checkAccess('moderate', 'access') || in_array($this->User->user_id, $this->_targetGroup))));

			if (!Switcher::check('ALL', $this->Ecosystem->getAlias() == Ecosystem::DOT_COM)) {
				$this->locationRedirect('/404.php');
			}
		}

		$this->_LandingManager = new LandingManager(LandingManager::CMS_LANDING_CONTENT_TYPE);
	}

	/**
	 * Внешняя страница лендинга
	 * 
	 * @author Dmitry Evtushenko
	 * @version 1.0, 27.02.2018
	 * 
	 * @param array $params Данные, переданные через строку
	 */
	public function index($params) {
		$urlname = $params[0];
		$this->locationRedirect('/', !$urlname);

		$postId = $this->_LandingManager->getPostIdByUrlname($urlname, $this->Ecosystem->getId());
		$this->locationRedirect('/404.php', !$postId);

		$this->setTemplate('/responsive/pages/landing/index.tpl');
		$this->_data['adaptive'] = true;

		//Логика сборки лендинга
		$this->_LandingManager->mode = LandingManager::LANDING_MODE_VIEW;
		$landingParams = $this->_LandingManager->getPostMainContent($postId);
		$this->locationRedirect('/404.php', $landingParams['status'] != LandingManager::LANDING_STATUS_PUBLISHED);

		$this->_data['page']['title'] = $landingParams['seo_title'];
		$this->_data['page']['description'] = $landingParams['seo_description'];
		$this->_data['page']['keywords'] = $landingParams['seo_keywords'];

		//Данные для Open Graph и шаринга
		$this->_data['page']['titleClear'] = $landingParams['og_title'];
		$this->_data['page']['descriptionClear'] = $landingParams['og_description'];
		$this->_data['page']['pageImage'] = '';
		$ogImage = $this->_LandingManager->getOgImage($postId);
		if ($ogImage['full_path']) {
			$this->_data['page']['pageImage'] = $ogImage['full_path'];
		}
		$this->_data['btns'] = Social::getSocialButtons('https://' . $this->Ecosystem->getDomainName() . '/landing/' . $urlname, $this->_data['page']['titleClear'], $this->_data['page']['descriptionClear'], $this->_data['page']['pageImage'], '', 2);
		$this->_data['page']['pageUrl'] = 'https://' . $this->Ecosystem->getDomainName() . '/landing/' . $urlname;

		//Параметры 
		$this->_LandingManager->languageId = $landingParams['language_id'];
		$this->_LandingManager->ecosystemId = ($landingParams['language_id'] == i18n::RUSSIA_LANGUAGE_ID) ? Ecosystem::DOT_RU_ID : Ecosystem::DOT_COM_ID;
		$this->_LandingManager->domenName = $this->domenName;

		//Блоки лендинга
		$this->_data['landingContent'] = $this->_LandingManager->getPostBlocks($postId);
		$this->_data['adaptiveItemPrefix'] = 'stpatric-item';
	}

	/**
	 * Список лендингов в конструкторе
	 * 
	 * @author Viktor Vorobyev
	 * @version 1.0, 27.02.2018
	 * 
	 * @param array $params Данные, переданные через строку
	 */
	public function list($params) {
		$this->setTemplate('/responsive/pages/landing/list.tpl');

		$this->_data['head'] = 'Конструктор лендингов';
		$this->_data['landings'] = $this->_LandingManager->getPostsList();
	}

	/**
	 * Страница создания нового лендинга
	 * 
	 * @author Viktor Vorobyev
	 * @version 1.0, 27.02.2018
	 * 
	 * @param array $params Данные, переданные через строку
	 */
	public function create($params) {
		$this->setTemplate('/responsive/pages/landing/edit.tpl');

		$this->_data['head'] = 'Создание лендинга';
		$this->_data['page']['pageType'] = 'create';
		$this->_LandingManager->mode = LandingManager::LANDING_MODE_CREATE;

		$clientData = [];
		$clientData['uploadImageUrl'] = Files::getHost(Files::SERVER_CMS_PREVIEW, Files::HOST_ACCEPTOR) . '/files/upload/cmsImages/';
		$clientData['landingsCount'] = LandingManager::getLandingsCount();
		$this->_data['clientData'] = $this->toJSON($clientData);
	}

	/**
	 * Страница редактирования лендинга
	 * 
	 * @author Viktor Vorobyev
	 * @version 1.0, 27.02.2018
	 * 
	 * @param array $params Данные, переданные через строку
	 */
	public function edit($params) {
		$urlname = $params[0];
		$this->locationRedirect('/landing/list', !$urlname);

		$postId = $this->_LandingManager->getPostIdByUrlname($urlname);
		$this->locationRedirect('/landing/list', !$postId);

		$this->setTemplate('/responsive/pages/landing/edit.tpl');

		$this->_data['head'] = 'Редактирование лендинга';
		$this->_data['page']['pageType'] = 'edit';
		$this->_LandingManager->mode = LandingManager::LANDING_MODE_EDIT;

		$landing = $this->_LandingManager->getPostMainContent($postId);
		$this->_data['landingParams'] = $landing;
		$this->_data['landingParams']['postId'] = $postId;
		$this->_data['landingParams']['urlname'] = $urlname;
		$this->_data['landingParams']['og_image'] = $this->_LandingManager->getOgImage($postId);

		$this->_LandingManager->languageId = $landing['language_id'];
		$this->_LandingManager->ecosystemId = ($landing['language_id'] == i18n::RUSSIA_LANGUAGE_ID) ? Ecosystem::DOT_RU_ID : Ecosystem::DOT_COM_ID;
		$this->_LandingManager->domenName = $this->domenName;
		
		global $betaversion;
		$this->_LandingManager->betaversion = $betaversion;

		//Блоки лендинга
		$this->_data['landingContent'] = $this->_LandingManager->getPostBlocks($postId);

		$clientData = [];
		$clientData['uploadImageUrl'] = Files::getHost(Files::SERVER_CMS_PREVIEW, Files::HOST_ACCEPTOR) . '/files/upload/cmsImages/';
		$clientData['landingsCount'] = LandingManager::getLandingsCount();
		$this->_data['clientData'] = $this->toJSON($clientData);
	}

	/**
	 * Страница предпросмотра
	 * 
	 * @author Viktor Vorobyev
	 * @version 1.0, 27.02.2018
	 * 
	 * @param array $params Данные, переданные через строку
	 */
	public function preview($params) {
		$this->setTemplate('/responsive/pages/landing/preview.tpl');

		$this->_data['head'] = 'Предпросмотр';
		$this->_data['page']['pageType'] = 'preview';
	}

	/**
	 * Ajax-метод сохранения данных лендинга
	 * 
	 * @author Dmitry Evtushenko
	 * @version 1.0, 11.04.2018
	 */
	public function save() {
		$this->locationRedirect('/', !$this->isAjax());

		$success = false;
		$postId = (int) $_POST['settings']['post-id'];
		$cBlocks = [];
		
		$languageId = (int) $_POST['settings']['language'];
		if (in_array($languageId, [i18n::ENGLISH_LANGUAGE_ID, i18n::ESPANIOL_LANGUAGE_ID])) {
			$this->_LandingManager->ecosystemId = Ecosystem::DOT_COM_ID;
		} else {
			$this->_LandingManager->ecosystemId = Ecosystem::DOT_RU_ID;
		}

		$urlName = $_POST['settings']['url'];
		if (!$urlName || strpos($urlName, '/landing/') === false) {
			$this->toJSON(['success' => $success, 'error' => 'URL лендинга задан некорректно'], true);
		}

		$urlNameGroup = explode('/landing/', $urlName);
		$friendlyUrl = array_pop($urlNameGroup);
		$friendlyUrl = ($friendlyUrl) ?? '';
		if (!Cms::checkPostUrlname($postId, $friendlyUrl)) {
			$this->toJSON(['success' => $success, 'error' => 'Лендинг с заданным URL уже существует'], true);
		}

		$settings = [];
		$settings['language_id'] = $languageId;
		$settings['name'] = $_POST['settings']['name'];
		$settings['user_id'] = (int) $this->User->user_id;
		$settings['status'] = (int) $_POST['settings']['status'];
		$settings['urlname'] = $friendlyUrl;
		$settings['landing_type'] = (int) $_POST['settings']['type'];
		$settings['title'] = $_POST['settings']['title'];
		$settings['description'] = $_POST['settings']['description'];
		$settings['keywords'] = $_POST['settings']['keywords'];
		$settings['og_title'] = $_POST['settings']['og-title'];
		$settings['og_description'] = $_POST['settings']['og-description'];
		$settings['og_image'] = [
			'file_id' => (int) $_POST['settings']['fid'],
			'server_id' => (int) $_POST['settings']['serverid'],
			'path' => (string) $_POST['settings']['path']
		];

		//Сборка блоков
		if (is_array($_POST['landing']) && count($_POST['landing']) > 0) {
			foreach($_POST['landing'] as $sortOrder => $block) {
				$blockType = (string) $block['type'];
				$blockId = (int) $block['blockId'];

				//Тип "Баннер"
				if ($blockType == Cms::TYPE_BLOCK_COMPOSITE) {
					$transformBlock = [
						'type' => $blockType,
						'blockId' => $blockId,
						'banner_type' => (int) $block['content'][0]['bannerType'],
						'text' => $block['content'][0]['title'],
						'substring' => $block['content'][0]['substring'],
						'link' => $block['content'][0]['link'],
						'file_id' => $block['content'][0]['fid'],
						'server_id' => $block['content'][0]['serverid'],
						'path' => $block['content'][0]['path'],
						'button_text' => $block['content'][0]['button-text'],
						'button_link' => $block['content'][0]['button-link'],
						'button_color' => str_replace('#', '', $block['content'][0]['button-color'])
					];

				//Тип "Коллаж"
				} elseif ($blockType == 'collage') {
					$transformBlock = [
						'type' => $blockType,
						'blockId' => $blockId,
						'cBlocks' => []
					];

					if (is_array($block['content']) && count($block['content']) > 0) {
						foreach($block['content'] as $sortOrder => $innerBlock) {
							$transformBlock['cBlocks'][$sortOrder]['type'] = $blockType;
							$transformBlock['cBlocks'][$sortOrder]['blockId'] = $innerBlock['blockId'];
							$transformBlock['cBlocks'][$sortOrder]['cBlocks'] = [];

							if (is_array($innerBlock['content']) && count($innerBlock['content']) > 0) {
								foreach($innerBlock['content'] as $key => $value) {
									$transformBlock['cBlocks'][$sortOrder]['cBlocks'][$key] = [
										'type' => Cms::TYPE_BLOCK_COMPOSITE,
										'blockId' => (int) $value['blockId'],
										'text' => $value['title'],
										'link' => $value['link'],
										'file_id' => $value['fid'],
										'server_id' => $value['serverid'],
										'path' => $value['path'],
									];
								}
							}
						}
					}
				
				//Тип "Текст"
				} elseif ($blockType == Cms::TYPE_BLOCK_TEXT) {
					$transformBlock = [
						'type' => $blockType,
						'blockId' => $blockId,
						'content' => $block['content'][0]['text']
					];

				//Тип "Блок шаринга"
				} elseif ($blockType == Cms::TYPE_BLOCK_SHARING) {
					$transformBlock = [
						'type' => $blockType,
						'blockId' => $blockId
					];

				//Тип "Товары"
				} elseif ($blockType == Cms::TYPE_BLOCK_TAGS) {
					$transformBlock = [
						'type' => $blockType,
						'blockId' => $blockId,
						'objectIds' => $this->_prepareObjects($block['content'][0]['items'])
					];
				}

				if (count($transformBlock)) {
					array_push($cBlocks, $transformBlock);
				}
			}
		}

		$Ecosystem = Ecosystem::getEcosystemByLanguageId($settings['language_id']);
		$oldUrl = $this->_LandingManager->getOldPostUrl($postId, $Ecosystem->getId());

		$response = $this->_LandingManager->saveLanding($postId, $settings, $cBlocks);

		if ($response['result']) {
			$sitemapData = ['landing', Sitemap::SITEMAP_LANDINGS];
			$sitemapLink = Sitemap::getMainLink($Ecosystem, [$sitemapData[0], $settings['urlname']]);
			$sitemapDataArray = [
				Sitemap::PARAM_URL_FIELD =>  $sitemapLink,
				Sitemap::PARAM_GROUP_FIELD => Sitemap::checkGroupId($postId),
				Sitemap::PARAM_ECOSYSTEM_FIELD => $Ecosystem->getId(),
				Sitemap::PARAM_ALTERNATE => Sitemap::generateAlternates($Ecosystem, $sitemapLink)
			];
			if (in_array($settings['status'], [LandingManager::LANDING_STATUS_PUBLISHED])) {
				$sitemapLinkOld = $oldUrl ? Sitemap::getMainLink($Ecosystem, [$sitemapData[0], $oldUrl]) : $sitemapLink;
				$sitemapDataArray[Sitemap::PARAM_ACTION] = Sitemap::ACTION_ADD;
				$sitemapDataArray[Sitemap::PARAM_URL_OLD_FIELD] = $sitemapLinkOld;
			} elseif (in_array($settings['status'], [LandingManager::LANDING_STATUS_DRAFT, LandingManager::LANDING_STATUS_ON_TIMER, LandingManager::LANDING_STATUS_SUSPENDED])) {
				$sitemapDataArray[Sitemap::PARAM_ACTION] = Sitemap::ACTION_DELETE;
			}
			Sitemap::appendActionToBroker($sitemapData[1], $sitemapDataArray);
		}
		$this->toJSON(['success' => $response['result'], 'error' => $response['error']], true);
	}

	/**
	 * Метод преобразует массив ссылок на работы в массив object_id работ
	 * 
	 * @author Dmitry Evtushenko
	 * @version 1.0, 11.04.2018
	 * 
	 * @param array $objectLinks Массив ссылок на работы
	 * @return array
	 */
	private function _prepareObjects($objectLinks) {
		$objectIds = [];
		if (is_array($objectLinks) && count($objectLinks) > 0) {
			foreach ($objectLinks as $link) {
				$linkParts = explode('/item/', $link);
				$objectId = (int) array_shift(explode('-', $linkParts[1]));
				if ($objectId > 0) {
					array_push($objectIds, $objectId);
				}
			}
		}
		return $objectIds;
	}

}
