import { resolveBaseURL } from 'js/responsive/modules/utilities/util';
import { autocompleteRate } from 'js/responsive/pages/help-center/utils/utils';
import { ZERO } from 'js/responsive/constants/common';

const SEARCH_TYPE = 7;
const MIN_LENGTH_SEARCH = 3;
const DEFAULT_LENGTH_SEARCH = 400;

class SupportService {
	constructor() {
		this.selectors = {
			eventSearchSubmit: '[data-event="searchSubmit"]',
			eventSearchEnter: '[data-event="searchEnter"]',
			eventSearchAutocomplete: '[data-event="searchAutocomplete"]',
			shortAnswer: '[data-autocomplete="shortAnswer"]',
			helperList: '[data-autocomplete="helperList"]',
			printTitle: '[data-event="printTitle"]',
			printShortAnswer: '[data-event="printShortAnswer"]',
			rateAsk: '[data-autocomplete="rateAsk"]',
			searchForm: '[name="help-search-form"]',
		};

		this.helper = $(this.selectors.eventSearchAutocomplete);
		this.shortAnswer = $(this.selectors.shortAnswer);
		this.helpLinks = $(this.selectors.helperList);
		this.printTitle = $(this.selectors.printTitle);
		this.printShortAnswer = $(this.selectors.printShortAnswer);
		this.rateAsk = $(this.selectors.rateAsk);
		this.bindEvents();
	}

	bindEvents() {
		$(document)
			.on('click touchend', this.selectors.eventSearchSubmit, event => this.searchSubmit(event))
			.on('input propertychange', this.selectors.eventSearchEnter, event => this.searchEnter(event))
			.on('keyup', this.selectors.eventSearchEnter, event => this.maxLengthLimit(event))
			.on('submit', this.selectors.searchForm, event => this.searchSubmit(event))
			.on('click', event => this.closeClickOnEnemy(event));
	}

	closeClickOnEnemy(event) {
		const $element = $('[data-event="searchAutocomplete"]');
		const $viewsList = $(event.target).closest(this.selectors.searchForm);
		const isInput = $(event.target).is(this.selectors.eventSearchEnter);

		if (!$viewsList.length) {
			$element.hide();
		} else if (isInput) {
			this.searchEnter(event);
		}
	}

	maxLengthLimit(event) {
		const $element = $(event.target);
		const maxLength = $element.attr('maxlength') || DEFAULT_LENGTH_SEARCH;
		$element.val($element.val().substr(ZERO, maxLength));
	}

	searchEnter(event) {
		const $element = $(event.target);
		const maxLength = $element.attr('maxlength') || DEFAULT_LENGTH_SEARCH;
		const searchString = $element.val().substr(ZERO, maxLength);
		if (searchString.trim().length > MIN_LENGTH_SEARCH) {
			this.sendData(searchString, SEARCH_TYPE);
			this.counterByWord(searchString);
		} else {
			this.helper.hide();
		}
	}

	searchSubmit(event) {
		const self = event.currentTarget;
		const $form = $(self).closest(this.selectors.searchForm);
		const $input = $form.find(this.selectors.eventSearchEnter);
		const valueSearch = $input.val();
		if (!valueSearch || valueSearch.trim() === '') {
			event.preventDefault();
		}
	}

	sendData(search, searchType) {
		$.ajax({
			url: resolveBaseURL('ajax/autocompleter.php'),
			type: 'post',
			dataType: 'json',
			data: {
				search: search,
				searchtype: searchType
			}
		}).done(data => {
			const needDelimeter = typeof data.shortAnswer !== 'undefined' && typeof data.searchLinks !== 'undefined';
			this.helpLinks.html('').toggleClass('help-search__helper-list--delimeter', needDelimeter);
			if (typeof data.shortAnswer !== 'undefined') {
				this.rateAsk.attr('data-article-id', data.shortAnswer[ZERO].id);
				this.printTitle.html(data.shortAnswer[ZERO].shortTitle);
				this.printShortAnswer.html(data.shortAnswer[ZERO].shortAnswer);
				this.shortAnswer.show();
				this.rateAsk.html(autocompleteRate(data.shortAnswer[ZERO].status));
			} else {
				this.shortAnswer.hide();
			}
			if (typeof data.searchLinks !== 'undefined') {
				this.helpLinks.html(
					data.searchLinks.map(element => {
						const link = element.link ? element.link : `/help/?article=${element.id}`;
						return `<li class="help-search__helper-item"><a href="${link}">${element.title}</a></li>`;
					}).join('')
				);

				this.helpLinks.show();
			} else {
				this.helpLinks.hide();
			}
			this.helper.show();
		}).fail(() => {
			this.helper.hide();
		});
	}

	counterByWord(searchString) {
		$.ajax({
			url: resolveBaseURL('/help/counterByWordInSearch'),
			data: {
				word: searchString
			},
			type: 'POST',
			dataType: 'json'
		});
	}
}

export default SupportService;