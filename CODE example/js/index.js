/**
 * Copyright (c) 2019-present, Livemaster, LLC.
 *
 * @module @lm/pages/hhelp-center
 * @version 0.1.0
 */

import 'js/responsive/base.js';
import 'css/responsive/pages/help-center/index.scss';

import FooterElement from 'js/responsive/modules/_elements/footer';
import HeaderElement from 'js/responsive/modules/_elements/header';
import { fireAsync } from 'js/responsive/modules/utilities/util';
import SearchHelpCenter from 'js/responsive/pages/help-center/search';
import Feedback from 'js/responsive/pages/help-center/feedback';

const initSearchHelpCenter = () => new SearchHelpCenter();
const initFeedback = () => new Feedback();

const init = () =>
	fireAsync([
		HeaderElement.init,
		FooterElement.init,
		initSearchHelpCenter,
		initFeedback,
	]);

document.addEventListener('DOMContentLoaded', init, false);
