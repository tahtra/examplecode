{# Хлебные крошки #}
<div class="row">
	<div class="col-xs-12 hidden-xs">
		{% include 'responsive/_elements/breadcrumbs/index.tpl' with { breadcrumbs: adapter.content.elemsData.breadcrumbs } %}
	</div>
</div>

<div class="row">
	{# Баннер #}
	<div class="col-xs-12">
		{% include 'responsive/pages/help-center/parts/banner.tpl' %}
	</div>

	{# Content #}
	<div class="col-xs-12 col-md-9">
		{# FAQ #}
		<div class="row">
			<div class="col-xs-12">
				{% include 'responsive/pages/help-center/parts/faq.tpl' with { 
					type: 'faq',
					title: 'Частые вопросы'|trans,
					articles: indexArticles,
				} %}
			</div>
		</div>

		{# Разделы #}
		<div class="row">
			<div class="col-xs-12">
				{% include 'responsive/pages/help-center/parts/sections.tpl' %}
			</div>
		</div>
	</div>

	{# Сайдбар #}
	<div class="col-xs-12 col-md-3 hidden-xs hidden-sm">
		{% include 'responsive/pages/help-center/parts/sidebar.tpl' %}
	</div>
</div>