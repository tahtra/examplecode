<div class="help-faq block">
	<div class="help-faq__title">
		<span class="help-faq__title-text help-faq__title-text--{{ type }}">{{ title }}</span>
	</div>

	<div class="help-faq__article-list">
		<div class="row">
			{% for article in articles %}
				{% if loop.first %}
					<div class="col-xs-12 col-sm-6">
				{% endif %}

				<div class="help-faq__item">
					<a href="{{ article.pageLink }}" class="help-faq__link">{{ article.title }}</a>
				</div>

				{% if loop.index == articles.halfLength %}
					</div>
					<div class="col-xs-12 col-sm-6">
				{% endif %}

				{% if loop.last %}
					</div>
				{% endif %}
			{% endfor %}
		</div>
	</div>
</div>