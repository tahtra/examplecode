<div class="block block--inner">
	<div class="row">
		{% for subId, subSec in subsections %}
			{% if subSec.onmain == 1 %}
				<div class="col-xs-12 col-sm-6 col-md-6 help-sections">
					<div class="help-sections__item">
						<div class="help-sections__section">
							<img class="help-sections__title-image" src="/image/responsive/pages/help-center/{{ subId }}.svg" width="48" height="48" alt="">
							<div class="help-sections__title">
								<a href="{{ subSec.pageLink }}" class="help-sections__title-link">{{ newTitleSection[subSec.id] ?: subSec.title }}</a>
								<div class="help-sections__title-description hidden-xs hidden-sm" title="{{ subSec.description|striptags }}">
									{{- subSec.description|striptags -}}
								</div>
							</div>
						</div>

						<div class="help-sections__subsections">
							{% set sections = subSec.recarticles|length > 0 ? subSec.recarticles : subSec.articles %}
							{% for article in sections|slice(0, 3) %}
								<div class="help-sections__subsections-item">
									<a href="{{ article.pageLink }}" class="help-sections__subsections-link">{{ article.title }}</a>
								</div>
							{% endfor %}
							<div class="help-sections__subsections-item">
								<a href="{{ subSec.pageLink }}" class="help-sections__subsections-link">{% trans %}Все темы{% endtrans %}&nbsp;></a>
							</div>
						</div>
					</div>
				</div> 
			{% endif %}
		{% endfor %}
	</div>
</div>
