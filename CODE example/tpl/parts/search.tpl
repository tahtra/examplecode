<form class="help-search" action="help/search/" method="get" name="help-search-form" autocomplete="off">
	<input type="hidden" name="searchtype" id="searchtype_help" value="7">
	<input
		class="help-search__input"
		name="search"
		type="text"
		placeholder="{% trans %}Поиск ответа{% endtrans %}"
		data-event="searchEnter"
		maxlength="400"
	>
	<button class="help-search__button btn btn--primary" type="submit" data-event="searchSubmit">{% trans %}Найти{% endtrans %}</button>
	<div class="help-search__autocomplete" data-event="searchAutocomplete">
		<div class="help-search__autocomplete-answer" data-autocomplete="shortAnswer">
			<div class="help-search__autocomplete-answer-title" data-event="printTitle"></div>
			<div class="help-search__autocomplete-answer-short" data-event="printShortAnswer"></div>
			<div class="rate-ask" data-autocomplete="rateAsk"></div>
		</div>
		
		<ul class="help-search__helper-list help-search__helper-list--delimeter" data-autocomplete="helperList"></ul>
	</div>
</form>
