<div class="help-faq block">
	<div class="help-faq__title">
		<span class="help-faq__title-text">{% trans %}Частые вопросы{% endtrans %}</span>
	</div>
	<div class="help-faq__content">
		{% for id, article in indexArticles %}
			<div class="help-faq__item">
				<a href="{{ article.pageLink }}" class="help-faq__link">{{ article.title }}</a>
			</div>
		{% endfor %}
	</div>
</div>