{% set id = shortAnswer.id ?: false %}
{% set id = articleId ?: id %}

<div class="rate-ask" {{ id ? 'data-article-id="' ~ id ~ '"' }} data-autocomplete="rateAsk">
	<div class="rate-ask__title">
		{%- trans %}Вам помог этот ответ?{% endtrans -%}
	</div>

	<div class="rate-ask__rate">
		<a class="rate-ask__link" data-event="autocompleteAnswer" data-type="true">{% trans %}Да{% endtrans %}</a>
		<a class="rate-ask__link" data-event="autocompleteAnswer" data-type="false">{% trans %}Нет{% endtrans %}</a>
	</div>
</div>