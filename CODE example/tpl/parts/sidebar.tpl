{% if index %}
	<div class="help-sidebar block block--inner">
		<div class="help-sidebar__title">
			{%- trans %}Новые статьи{% endtrans -%}
		</div>

		<div class="help-sidebar__list">
			{% for article in indexNewArticles %}
				<div class="help-sidebar__item">
					<a href="{{ article.pageLink }}" class="help-sidebar__link">{{ article.title }}</a>
				</div>
			{% endfor %}
		</div>
	</div>
{% else %}
	<div class="help-sidebar block block--inner">
		<div class="help-sidebar__title">
			<div class="help-sidebar__title-link">
				{%- trans %}Разделы{% endtrans -%}
			</div>
		</div>

		<div class="help-sidebar__list">
			{% for id, sec in sections %}
				{% if (sec.parent == 0) and ( not sec.hidden ) %}
					<div class="help-sidebar__item">
						<a href="{{ sec.pageLink }}" class="help-sidebar__link{{ sectionId == id ? ' help-sidebar__link--select'}}">{{ newTitleSection[sec.id] ?: sec.title }}</a>
					</div>
				{% endif %}
			{% endfor %}
		</div>
	</div>
{% endif %}
