{# Хлебные крошки #}
<div class="row">
	<div class="col-xs-12 hidden-xs">
		{% include 'responsive/_elements/breadcrumbs/index.tpl' with { breadcrumbs: adapter.content.elemsData.breadcrumbs } %}
	</div>
</div>

<div class="row">
	{# Баннер #}
	<div class="col-xs-12">
		{% include 'responsive/pages/help-center/parts/banner.tpl' %}
	</div>

	{# Content #}
	<div class="col-xs-12 col-md-9">
		{% if article %}
			{# Страница статьи #}
			<div class="row">
				<div class="col-xs-12">
					{# Статья #}
					<div class="block block--inner">
						{% if not article.formal %}
							<h2 class="help-title">{{ article.title }}</h2>
						{% endif %}
						<div class="help-content">{{ article.text }}</div>

						{% if votingStatus == 2 and not article.formal %}
							{% include 'responsive/pages/help-center/parts/feedback.tpl' %}
						{% elseif votingStatus == 1 %}
							<div class="rate-ask">
								<div class="rate-ask__title">
									{% trans %}Спасибо! Ваш ответ учтен.{% endtrans %}
								</div>
							</div>
						{% endif %}
					</div>

					{% include 'responsive/pages/help-center/parts/not-enouth.tpl' %}
				</div>
			</div>
		{% else %}
			{# Страница раздела #}
			<div class="row">
				<div class="col-xs-12">
					{# Заголовок #}
					<h2 class="help-title">{{ sections[curSection].title }}</h2>
				</div>
			</div>

			{% if recarticles|length > 1 %}
				<div class="row">
					<div class="col-xs-12">
						{# FAQ #}
						{% include 'responsive/pages/help-center/parts/faq.tpl' with { 
							type: 'recommended',
							title: 'Рекомендованные статьи'|trans,
							articles: recarticles,
						} %}
					</div>
				</div>
			{% endif %}

			<div class="row">
				<div class="col-xs-12">
					{# Статьи в текущей категории #}
					{% if articles %}
						<div class="help-article block block--inner">
							{% for id, article in articles[curSection] %}
								<div class="help-article__item {{ (article.priority and loop.index <= 3) or loop.first ? 'help-article__item--priority' }}">
									<a href="{{ article.pageLink }}" class="help-article__link">{{ article.title }}</a>
								</div>
							{% endfor %}
						</div>
					{% endif %}

					{# Статьи #}
					{% for subId, subSec in subsections %}
						{% if (subsections|length >= 1) and (subarticles[subId]) %}
							<div class="help-article block block--inner">
								<div class="help-article__title">
									{{- subSec.title -}}
								</div>

								{% for id, article in subarticles[subId] %}
									<div class="help-article__item {{ (article.priority and loop.index <= 3) or loop.first ? 'help-article__item--priority' }}">
										<a href="{{ article.pageLink }}" class="help-article__link">{{ article.title }}</a>
									</div>
								{% endfor %}
							</div>
						{% endif %}
					{% endfor %}

					{% include 'responsive/pages/help-center/parts/not-enouth.tpl' %}
				</div>
			</div>
		{% endif %}
	</div>

	{# Сайдбар #}
	<div class="col-xs-12 col-md-3 hidden-xs hidden-sm">
		{% include 'responsive/pages/help-center/parts/sidebar.tpl' %}
	</div>
</div>
