{% extends "responsive/_layout/base.tpl" %}

{% block styles %}
	<link rel="stylesheet" href="{{ webpackAsset('help-center.css') }}">
{% endblock %}

{% block content %}

	<div class="container-fluid">
		{% if index %}
			{% include 'responsive/pages/help-center/main.tpl' %}
		{% elseif search %}
			{% include 'responsive/pages/help-center/search.tpl' %}
		{% else %}
			{% include 'responsive/pages/help-center/article.tpl' %}
		{% endif %}
	</div>

{% endblock %}

{% block scripts %}
	<script id="client-data" type="text/json">{{ adapter.clientData }}</script>
	<script src="{{ webpackAsset('help-center.js') }}"></script>
{% endblock %}
