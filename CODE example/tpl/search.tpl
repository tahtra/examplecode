{# Хлебные крошки #}
<div class="row">
	<div class="col-xs-12 hidden-xs">
		{% include 'responsive/_elements/breadcrumbs/index.tpl' with { breadcrumbs: adapter.content.elemsData.breadcrumbs } %}
	</div>
</div>

<div class="row">
	{# Баннер #}
	<div class="col-xs-12">
		{% include 'responsive/pages/help-center/parts/banner.tpl' %}
	</div>

	{# Content #}
	<div class="col-xs-12 col-md-9">
		{# FAQ #}
		<div class="row">
			<div class="col-xs-12">
				{% if shortAnswer %}
					<div class="block block--inner">
						<h2 class="help-title">{{ shortAnswer.title }}</h2>
						<div class="help-content">{{ shortAnswer.text }}</div>

						<div class="is-this-answer-help-you">
							{% if shortAnswerStatus == 2 %}
								{% include 'responsive/pages/help-center/parts/feedback.tpl' %}
							{% else %}
								<div class="rate-ask">
									<div class="rate-ask__title">
										{% trans %}Спасибо! Ваш ответ учтен.{% endtrans %}
									</div>
								</div>
							{% endif %}
						</div>
					</div>
				{% endif %}

				{% if searchResults.output or shortAnswer %}
					<div class="block block--inner">
						<div class="help-center__row">
							{% for item in searchResults.output %}
								<div class="answer">
									<div class="answer__title">
										{% set href = 'help.php?article=' ~ item.id %}
										{%- if item.link -%}
											{% set href = item.link %}
										{%- elseif item.friendlyURL -%}
											{% set href = 'help/' ~ item.friendlyURL %}
										{%- endif -%}
										<a href="{{ href }}" class="answer__title-link">{{ item.title ~ '&nbsp;>' }}</a>
									</div>
									<div class="answer__description">{{ item.intro }}</div>
								</div>
							{% endfor %}
							{% if searchPagesCount > 1 %}
								{% include "responsive/_elements/pagebar/index.tpl" with {
									pageBar: adapter.content.elemsData.pagination.pageBar,
									linkPrev: adapter.content.elemsData.pagination.mobileLinks.prev,
									linkNext: adapter.content.elemsData.pagination.mobileLinks.next,
								} %}
							{% endif %}
						</div>
					</div>

					{% include 'responsive/pages/help-center/parts/not-enouth.tpl' %}
				{% else %}
					{% if searchedText %}
						{% set searchedText %}<strong>«{{ searchedText }}»</strong>&nbsp;{% endset %}
					{% else %}
						{% set searchedText = '' %}
					{% endif %}
					<div class="plug">
						<div class="plug__image">
							<img src="/image/responsive/svg/pages/order/plug-all.svg" alt="" align="center" width="155" height="163">
						</div>
						<div class="plug-text">
							{% trans %}По Вашему запросу {{ searchedText }}ничего не найдено.<br> Проверьте правильность написания или попробуйте перефразировать запрос.<br>
							Если Вы все же не нашли нужного ответа — <a href="questions/33">напишите нам</a>, мы обязательно поможем!{% endtrans %}
						</div>
					</div>
				{% endif %}
			</div>
		</div>
	</div>

	{# Сайдбар #}
	<div class="col-xs-12 col-md-3 hidden-xs hidden-sm">
		{% include 'responsive/pages/help-center/parts/sidebar.tpl' %}
	</div>
</div>
